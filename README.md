awcolor
=========

Welcome to the home for the Corona SDK Plugin awcolor.

awcolor is A color plugin for Corona SDK.
This plugin helps you call color percentage values for use with Corona's Graphic 2.0.

This git repo is home to the working lua module which is not the same as the final packaged plugin that is submitted to Corona. This is a work in progress module.
See version history below.

Versions
=========

The current version submitted to Corona is **1.0**

Version | Details
------------- | -------------
1.1.01-07-2015 | Bug Fixes
1.1  | Added function loadColorListJSON()
1.0 **(release)** | Added function setColorListLUA()
0.3  | Added .random() function
0.2 | Added X11 Color List and .fromHex() function
0.1  | First Release 


Basic Usage
=========

For a full breakdown of the usage of this mod please visit the wiki. [https://bitbucket.org/aged/mod_color/wiki/Home](https://bitbucket.org/aged/mod_color/wiki/Home)

Bugs & Features
=========
I am looking to always improve this module so please feel free to add feature requests of bug requests. [https://bitbucket.org/aged/mod_color/issues?status=new&status=open](https://bitbucket.org/aged/mod_color/issues?status=new&status=open)