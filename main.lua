-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

--- Import mod_color
local color = require("mod_color")

--- Setup a colorList from a lua table
local c = require("xelevenlist")
color:setColorListLUA( c )

--- Example 1 
--
-- fromString()
-- Pass through a string conatining the name of the color you wish to use.
local rec = display.newRect( 50, 50, 50, 50 )
rec:setFillColor( color.fromString("steelblue") )

--- Example 2
-- 
-- fromRGB()
-- Pass through a graphics 1.0 RGB Value and get a Graphics 2.0 Percentage value in return
local rec2 = display.newRect( 125, 50, 50, 50 )
rec2:setFillColor( color.fromRGB( 187, 187, 187 ) )

--- Example 3
--
-- fromHex()
-- Pass through a hex value and get a Graphics 2.0 Percentage value in return
local rec3 = display.newRect( 200, 50, 50, 50 )
rec3:setFillColor( color.fromHex("#FFCCFDD") )

--- Example 4
--
-- random()
-- ask for a random color
local rec4 = display.newRect( 50, 125, 50, 50 )
rec4:setFillColor( color.random() )

--- Setup a colorList from a JSON file
color:loadColorlistJSON("xelevenlist.JSON")

--- Example 5
-- 
-- This examples shows us using the colorList once a JSON file is loaded
local rec5 = display.newRect( 125, 125, 50, 50 )
rec5:setFillColor( color.fromString("lightgreen") )
